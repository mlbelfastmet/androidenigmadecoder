﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using Google.Android.Material.Snackbar;
using System.Text;

namespace MyAndroidApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        public AppCompatEditText mEditText;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            AppCompatButton btnDecrypt = FindViewById<AppCompatButton>(Resource.Id.btnDecrypt);
            btnDecrypt.Click += DecryptOnClick;

            mEditText = FindViewById<AppCompatEditText>(Resource.Id.etSecret);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void DecryptOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            string decodedString = "Failed to decode";

            // Base 64 decode the string supplied
            string encodedString = mEditText.Text;
            byte[] data;
            try
            {
                data = Convert.FromBase64String(encodedString);
                decodedString = Encoding.UTF8.GetString(data);
            } catch (Exception e)
            {
            }
            
            Snackbar.Make(view, decodedString, Snackbar.LengthLong)
                .SetAction("Action", (View.IOnClickListener)null).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
	}
}
